//
//  ViewController.swift
//  I Am Poor
//
//  Created by CommudePH0160 on 8/9/19.
//  Copyright © 2019 CommudePH0160. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var switchButton: UIButton!
    @IBOutlet weak var richImageView: UIImageView!
    @IBOutlet weak var poorImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    var isRich = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        switchStatus(status: isRich)
        
    }

    @IBAction func switchButtonTapped(_ sender: Any) {
        
        switchStatus(status: isRich)
    }
    
    func switchStatus (status: Bool) {
        
        if isRich {
            statusLabel.text = "I am Rich"
            richImageView.isHidden = false
            poorImageView.isHidden = true
        } else {
            statusLabel.text = "I am Poor"
            richImageView.isHidden = true
            poorImageView.isHidden = false
        }
        
        isRich = !status
    }
    

}

